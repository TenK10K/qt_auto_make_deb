#!/bin/bash
#read me: edit project_name qt_load_path qt_sh_path to your own path (qt_load_path is the version path. ex:5.14.2)

#create dir
project_name="demo"
qt_load_path="/home/a/Qt5.14.2/5.14.2/"
qt_project_bin="/home/a/Qt5/build-demo-Desktop_Qt_5_14_2_GCC_64bit-Release/demo"

bin_path="opt/"$project_name"/bin"
sh_path_name=$bin_path/$project_name.sh
icon_path="usr/share/icons/"$project_name

rm -rf $project_name

mkdir -p $project_name/DEBIAN
mkdir -p $project_name/$bin_path
mkdir -p $project_name/usr/share/applications
mkdir -p $project_name/$icon_path


cd $project_name

#create Qt APP.sh under the bin file
echo "#!/bin/sh  
appname=\`basename \$0 | sed s,\.sh$,,\`
dirname=\`dirname \$0\`
tmp=\"\${dirname#?}\"
if [ \"\${dirname%\$tmp}\" != \"/\" ]; then
dirname=\$PWD/\$dirname
fi
LD_LIBRARY_PATH=\$dirname
export LD_LIBRARY_PATH
\$dirname/\$appname \"\$@\"
"> $sh_path_name
chmod +x $sh_path_name

#create control (you should edit it)
echo "Package:$project_name
Version:1.0.0
Architecture:amd64
Section:utils
Maintainer: yolo
Priority:optional
Description: a test
"> DEBIAN/control

#create postinst
echo "#! /bin/sh

chmod -R 755 /opt/$project_name/bin

# 如果程序已经在运行，先杀掉程序，可解决覆盖安装问题
processLogin=\$(ps aux | grep '$project_name' | grep -v grep)
if [ \"\$processLogin\" != \"\" ]; then
	    pkill $project_name
fi

# 下面主要是把启动程序拷贝到桌面
NAME_USER=\`who | awk '{print \$1}' | sort | uniq\`
for name in \$NAME_USER;

do
	if [ \"\$name\" != \"root\" ];then
		CURRENT_HOME_DIR=\"/home/\$name\"
	else
		CURRENT_HOME_DIR=\"/\$name\"
	fi
	DESKTOP_DIR_CH=\$CURRENT_HOME_DIR/Desktop
	APPPATH=\"/opt/$project_name/bin\"

	if [ -d \$DESKTOP_DIR_CH ]; 
	then
		cp /usr/share/applications/$project_name.desktop \$DESKTOP_DIR_CH
		chmod +w \$DESKTOP_DIR_CH/$project_name.desktop
		chown \$name:\$name \$DESKTOP_DIR_CH/$project_name.desktop
		chown \$name:\$name \$APPPATH
	fi

done
"> DEBIAN/postinst.sh

#create DEBIAN prerm
echo "#! /bin/sh

# 如果程序在运行，先杀掉程序
processLogin=\$(ps aux | grep \'$project_name\' | grep -v grep)
if [ \"\$processLogin\" != \"\" ]; then
	    pkill $project_name
fi

# 清理文件
rm -rf /opt/$project_name/*

# 移除桌面启动程序
NAME_USER=\`who | awk \'{print \$1}\' | sort | uniq\`
for name in \$NAME_USER
do
	if [ \"\$name\" != \"root\" ]; then
		CURRENT_HOME_DIR=\"/home/\$name\"
	else
		CURRENT_HOME_DIR=\"/\$name\"
	fi

	DESKTOP_CH=\$CURRENT_HOME_DIR/Desktop/$project_name.desktop

	echo \$DESKTOP_CH
	if [ -f \$DESKTOP_CH ]; then
		rm -rf \$DESKTOP_CH
	fi
done
" > DEBIAN/prerm.sh

#create desktop file (you can edit it)
echo "[Desktop Entry]
Version=1.0.0
Name=$project_name
Comment=$project_name application
Name[zh_CN]=$project_name
Comment[zh_CN]=$project_name
Exec=/opt/$project_name/bin/$project_name.sh
Icon=/usr/share/icons/$project_name/$project_name.ico
Terminal=false
Type=Application
Categories=Qt;desktop
StartupNotify=true
MimeType=application/x-desktop;
Keywords=$project_name;
" > usr/share/applications/$project_name.desktop

#copy Qt Applications to bin
cp $qt_project_bin $bin_path/$project_name

ldd $bin_path/$project_name | awk '{print $3}' | xargs -i cp -L {} $bin_path

#add other dependcy to bin
cd ../

cp -rf $qt_load_path/gcc_64/plugins/platforms $project_name/$bin_path
cp -rf $qt_load_path/gcc_64/plugins/imageformats $project_name/$bin_path

if [ -f $qt_load_path/gcc_64/plugins/platforms/lib/libQt5DBus.so.5 ]; then

    cp $qt_load_path/gcc_64/plugins/platforms/lib/libQt5DBus.so.5 $project_name/$bin_path

else

    cp ./libQt5DBus.so.5 $project_name/$bin_path	

fi

if [ -f $qt_load_path/gcc_64/plugins/platforms/lib/libQt5XcbQpa.so.5 ]; then

    cp $qt_load_path/gcc_64/plugins/platforms/lib/libQt5XcbQpa.so.5 $project_name/$bin_path

else

    cp ./libQt5XcbQpa.so.5 $project_name/$bin_path

fi



#add ico
cp $project_name.ico $project_name/$icon_path/$project_name.ico

#dpkg order
dpkg -b $project_name $project_name.deb





