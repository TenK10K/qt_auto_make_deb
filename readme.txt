注意事项：
1.将程序图标放入qt_auto_make_deb，ico格式（命名与Qt项目名称一致）
2.编辑package.sh文件中的project_name、qt_load_path、qt_project_bin
    project_name       ： Qt项目名称
    qt_load_path       ： Qt安装路径的版本号目录路径,如"/home/xxx/Qt5.14.2/5.14.2/"
    qt_project_bin     ： Qt项目以Release生成的可执行文件的路径,如"/home/xxx/Qt5/build-demo-Desktop_Qt_5_14_2_GCC_64bit-Release/project_name" 
    DEBIAN/control     ： 文件以描述软件版本等的信息
    usr/share/application/desktop      ： 描述桌面程序相关信息
3.qt_auto_make_deb目录下运行命令:  ./package.sh ，生成对应的deb文件
